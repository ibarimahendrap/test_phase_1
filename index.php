<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 
        Nama    = Ida Bagus Ari Mahendra Putra
        Email   = ibarimahendrap@gmail.com
        No      = 081339001359 
    -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test Phase 1 - Island Media Management</title>

    <link rel="stylesheet" href="style.css">

    <?php
        $arr_month  = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $today      = date('l');
    ?>
</head>
<body>
    <header></header>
    <main>
        <div class="month-body">
            <div class="container">
                <p>Today is <span class="today"><?=$today?></span></p>
                <div class="list-month">
                    <?php
                        for ($i=0; $i < count($arr_month); $i++) : 
                            $month      = $arr_month[$i];
                            $setToday   = $month == $today ? 'active' : '';
                            ?>

                            <button class="btn btn-month <?=$setToday?>" value="<?=$month?>" onclick="setBtn(this)"><?=$month?></button>

                            <?php
                        endfor;
                    ?>
                </div>
                <p>Selected day is <span class="selected">. . .</span></p>
            </div>
        </div>
    </main>

    <script src="script.js"></script>
    
</body>
</html>